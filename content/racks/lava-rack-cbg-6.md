---
title: lava-rack-cbg-6
---

# [lava-rack-cbg-6](https://lava.collabora.co.uk/scheduler/worker/lava-rack-cbg-6) Dispatcher and controller

Dispatcher is [Cyberserve Xeon E-RS100-E10](https://www.broadberry.co.uk/xeon-e-rackmount-servers/cyberserve-rs100-e10-pi2) 
and the server specification can be found on one of [the quotations from broadberry.](/files/PDAR52995.pdf) 
The server can be accessed over SSH and has the IP address of 10.154.97.106. 
The switch for the rack is a [Zyxel GS1900-24](https://www.zyxel.com/uk/en/products_services/8-10-16-24-48-port-GbE-Smart-Managed-Switch-GS1900-Series/specification) 
and is on IP address 10.154.97.206. 
The USB hub is an [Exsys EX-1116HMVS](https://www.exsys-shop.de/shopware/en/categories/usb-products/510/usb-3.2-gen1-metal-hub-with-16-ports-din-rail-15kv-esd-surge-protection) 
and is connected to a [24vDC power adapter](https://uk.farnell.com/tracopower/txm100-124/power-supply-ac-dc-24v-4-2a/dp/2929691)


# Devices from top to bottom

[hp-x360-12b-ca0010nr-n4020-octopus-cbg-4](https://lava.collabora.co.uk/scheduler/device/hp-x360-12b-ca0010nr-n4020-octopus-cbg-4) 
power connected to the [vertical power bar](https://www.racksolutions.co.uk/vertical-rack-mount-power-strips-8.html), 
connection to USB hub using a [SuzyQable](https://www.sparkfun.com/products/retired/14746), 
connection to the switch using network cable and [TechRise USB Ethernet adapter](https://www.amazon.co.uk/TechRise-Network-Adapter-Gigabit-Ethernet/dp/B087PCKGVW/ref=sr_1_3?dchild=1&keywords=techrise+USB+ethernet+adapter&qid=1621331512&s=computers&sr=1-3). 

[dell-latitude-5400-8665U-sarien-cbg-0](https://lava.collabora.co.uk/scheduler/device/dell-latitude-5400-8665U-sarien-cbg-0) 
power connected to the vertical power bar, 
connection to the USB hub using [servoMicro](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_micro.md) 
and a [USB type A to usb Micro cable](https://cpc.farnell.com/pro-signal/psg91471/lead-usb2-0-a-male-micro-b-male/dp/CS30867),
connection to the switch using ethernet cable and TechRise USB Ethernet adapter 
and another cable from the onboard ethernet to the switch.

Space.

[sc7180-trogdor-lazor-limozeen-cbg-4](https://lava.collabora.co.uk/scheduler/device/sc7180-trogdor-lazor-limozeen-cbg-4) 
power connected to the vertical power bar, 
connection to the USB hub using a SuzyQable, 
connection to the switch using ethernet cable and [Asix AX88772B USB Ethernet adapter](https://www.amazon.co.uk/Ethernet-Adapter-AX88772B-Converter-Micro-USB-RJ45/dp/B085X8BJMY/).

[asus-C436FA-Flip-hatch-cbg-4](https://lava.collabora.co.uk/scheduler/device/asus-C436FA-Flip-hatch-cbg-4) 
power connected to the servov4, 
connection to the USB hub using a [servov4](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_v4.md) 
and a [USB type A to usb Micro cable](https://cpc.farnell.com/pro-signal/psg91471/lead-usb2-0-a-male-micro-b-male/dp/CS30867), 
connection to the switch using a network cable from the servov4. 

This shelf contains the [power supply](https://uk.farnell.com/tracopower/txh-240-124/psu-ac-to-dc-24v-240w/dp/1772184) for the USB Hub.

Server connected to the [horizontal power bar](https://www.racksolutions.co.uk/catalog/product/view/id/2722/s/8-way-13-amp-horizontal-rack-mount-power-strips/), 
USB connection to the USB hub and network connection to the switch. 
Configured using [chef](https://gitlab.collabora.com/lava/collabora-lava-setup)

USB hub connected to 24vDC power Adapter (in turn connected to the horizontal power bar), 
Main USB3 type-B to the Disptcer Type-A and then to each of the devices in the rack.

Switch Power connected to the horizontal power bar, connection to the core switch, 
connection to the dispatcher and then to each of the devices. 
Configuration found [here](https://gitlab.collabora.com/lava/collabora-lava-setup/-/blob/master/doc/rack-assembly.md#managed-ethernet-switch)

Space.

[acer-R721T-grunt-cbg-1](https://lava.collabora.co.uk/scheduler/device/acer-R721T-grunt-cbg-1) 
power connected to the vertical power bar, 
connection to the USB hub using a SuzyQable 
and a [Ykush XS switchable USB hub](https://www.yepkit.com/product/300115/YKUSHXS), 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter.

[lenovo-TPad-C13-Yoga-zork-cbg-4](https://lava.collabora.co.uk/scheduler/device/lenovo-TPad-C13-Yoga-zork-cbg-4) 
power connected to the vertical power bar, 
connection to the USB hub using a SuzyQable, 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter.

[asus-C523NA-A20057-coral-cbg-6](https://lava.collabora.co.uk/scheduler/device/asus-C523NA-A20057-coral-cbg-6) 
power connected to the servov4p1, 
connection to the USB hub using a [servov4p1](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_v4p1.md) 
and a SuzyQable, 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter. 

[hp-11A-G6-EE-grunt-cbg-6](https://lava.collabora.co.uk/scheduler/device/hp-11A-G6-EE-grunt-cbg-6) 
power connected to the vertical power bar, 
connection to the USB hub using a SuzyQable, 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter.

[acer-cb317-1h-c3z6-dedede-cbg-2](https://lava.collabora.dev/scheduler/device/acer-cb317-1h-c3z6-dedede-cbg-2) 
power connected to the vertical power bar, 
connection to the USB hub using a SuzyQable, 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter.

[rk3399-gru-kevin-cbg-0](https://lava.collabora.co.uk/scheduler/device/rk3399-gru-kevian-cbg-0) 
power connected to the vertical power bar, 
connection to the USB hub using servoMicro and a USB type A to usb Micro cable, 
connection to the switch using a network cable and a Asix AX88772B USB Ethernet adapter.

[dell-latitude-5400-4305U-sarien-cbg-1](https://lava.collabora.co.uk/scheduler/device/dell-latitude-5400-4305U-sarien-cbg-1) 
power connected to the vertical power bar, 
connection to the USB hub using servoMicro and a USB type A to usb Micro cable, 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter 
and another cable from the onboard ethernet to the switch.
