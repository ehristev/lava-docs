---
title: Nyan Chromebooks
---

`nyan` is a board name for ARM Chromebooks based on the Tegra K1 T124 SoC.

The Collabora LAVA lab contains the following `nyan` devices:

-   [Acer Chromebook 13 (CB5-311)](https://www.chromium.org/chromium-os/developer-information-for-chrome-os-devices/acer-cb5-311-chromebook-13) (codename `big`)
    -   See [tegra124-nyan-big](https://lava.collabora.dev/scheduler/device_type/tegra124-nyan-big) in LAVA
    -   CPU: 4x Arm Cortex-A15 2.1-2.5 GHz
    -   GPU: Nvidia GK20A
    -   SoC: Nvidia Tegra K1 (T124)
    -   Aarch: Arm

Mainline Linux kernel support was added in release v3.18-rc1: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/arch/arm/boot/dts/tegra124-nyan-big.dts?h=v3.18-rc1>

### Debugging interfaces

`nyan` boards have been flashed and tested with [ServoV2](../../01-debugging_interfaces).

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Asix AX88772B AX88772 USB-Eth adapter.

#### Known issues

See [Common issues](../common_issues.md).

### Example kernel command line arguments

An example of kernel command line arguments to boot a FIT image:

    console=ttyS0,115200n8 root=/dev/ram0 ip=dhcp tftpserverip=<server_ip>
