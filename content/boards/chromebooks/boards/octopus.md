---
title: Octopus Chromebooks
---

`octopus` is a board name for x86_64-based Chromebooks. Many vendors make
Chromebooks based on this board, some examples:

-   [Acer Chromebook
    311](http://eu-acerforeducation.acer.com/resources/bett-show-2019-acer-unveils-new-suite-of-11-6-inch-chromebooks/)
-   [Lenovo Chromebook
    S340-14](https://psref.lenovo.com/Product/Lenovo_Laptops/Lenovo_Chromebook_S340)
-   [Samsung Chromebook
    4](https://www.samsung.com/us/computing/chromebooks/under-12/chromebook-4-11-6-32gb-storage-4gb-ram-xe310xba-k01us/)
-   [HP x360 12b-ca0004na](https://support.hp.com/hk-en/document/c06608505)

These chromebooks use 64 bit Intel Gemini Lake processors like the Intel
Celeron N4000 and Celeron N4100. The rest of the specs may vary between
vendors and models.

The Collabora LAVA lab contains the following `octopus` devices:

-   [HP x360 12b-ca0010nr](https://www.intel.com/content/www/us/en/products/systems-devices/sku/h75688624/hp-chromebook-x360-12bca0010nr/specifications.html) (codename `bloog`)
    -   See [hp-x360-12b-ca0010nr-n4020-octopus](https://lava.collabora.dev/scheduler/device_type/hp-x360-12b-ca0010nr-n4020-octopus) in LAVA
    -   CPU: Intel Celeron N4020 CPU @ 1.10GHz
    -   GPU: GeminiLake [UHD Graphics 600]
    -   Arch: x86_64
-   [HP x360	12b-ca0500na](https://support.hp.com/us-en/document/c06506319) (codename `bloog`)
    -   See [hp-x360-12b-ca0500na-n4000-octopus](https://staging.lava.collabora.dev/scheduler/device_type/hp-x360-12b-ca0500na-n4000-octopus) in LAVA (staging instance only)
    -   CPU: Intel Celeron N4000 CPU @ 1.10GHz
    -   GPU: GeminiLake [UHD Graphics 600]
    -   Arch: x86_64

### Debugging interfaces

`octopus` boards have been flashed and tested with both [SuzyQ and Servo
v4](../../01-debugging_interfaces) interfaces.

In the HP Chromebook x360 model 12b-ca0010nr the debug interface is the
left USB-C port.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

We observed the following issues:

-   The R8152 Ethernet driver in Depthcharge doesn't seem reliable when
    working at Gigabit speeds. It's recommeded to configure the link as
    Fast Ethernet when booting over TFTP.
-   Depending on the firmware version of the Servo v4, Depthcharge hangs
    while initializing the Ethernet interface. From the Servo v4 FW
    versions we have tested, `servo_v4_v1.1.5690-46ed8a0 2016-12-01
    06:28:35 @build268-m2` is known to cause issues. Version
    `servo_v4_v2.3.22-ecb74cc56 2019-07-23 18:07:15
    @chromeos-legacy-release-us-central2-c-x32-40-2efr` works fine.

### Example kernel command line arguments

    earlyprintk=uart8250,mmio32,0xfed00000,115200n8 console=ttyS1,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug

the IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `bloog` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/image-bloog-R72-11297.83.47-octopus.dev-collabora-20220622.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `hp-x360-12b-ca0010nr-n4020-octopus`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      CPU [empty]
    /0/1                   memory         4GiB System memory
    /0/1/0                 memory         1GiB LPDDR4 Synchronous 2400 MHz (0.4 ns)
    /0/1/1                 memory         1GiB LPDDR4 Synchronous 2400 MHz (0.4 ns)
    /0/1/2                 memory         1GiB LPDDR4 Synchronous 2400 MHz (0.4 ns)
    /0/1/3                 memory         1GiB LPDDR4 Synchronous 2400 MHz (0.4 ns)
    /0/2                   processor      Intel(R) Celeron(R) N4020 CPU @ 1.10GHz
    /0/100                 bridge         Gemini Lake Host Bridge
    /0/100/2               display        GeminiLake [UHD Graphics 600]
    /0/100/c               network        AC 1550i Wireless
    /0/100/1f              bridge         Celeron/Pentium Silver Processor PCI-defau
    /1             eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Gemini Lake Host Bridge (rev 06)
    00:00.1 Signal processing controller: Intel Corporation Celeron/Pentium Silver Processor Dynamic Platform and Thermal Framework Processor Participant (rev 06)
    00:02.0 VGA compatible controller: Intel Corporation GeminiLake [UHD Graphics 600] (rev 06)
    00:0c.0 Network controller: Intel Corporation AC 1550i Wireless (rev 06)
    00:0e.0 Multimedia audio controller: Intel Corporation Celeron/Pentium Silver Processor High Definition Audio (rev 06)
    00:15.0 USB controller: Intel Corporation Celeron/Pentium Silver Processor USB 3.0 xHCI Controller (rev 06)
    00:17.0 Signal processing controller: Intel Corporation Device 31b4 (rev 06)
    00:17.1 Signal processing controller: Intel Corporation Device 31b6 (rev 06)
    00:17.2 Signal processing controller: Intel Corporation Device 31b8 (rev 06)
    00:17.3 Signal processing controller: Intel Corporation Device 31ba (rev 06)
    00:18.0 Signal processing controller: Intel Corporation Celeron/Pentium Silver Processor Serial IO UART Host Controller (rev 06)
    00:18.2 Signal processing controller: Intel Corporation Celeron/Pentium Silver Processor Serial IO UART Host Controller (rev 06)
    00:19.0 Signal processing controller: Intel Corporation Celeron/Pentium Silver Processor Serial IO SPI Host Controller (rev 06)
    00:19.2 Signal processing controller: Intel Corporation Celeron/Pentium Silver Processor Serial IO SPI Host Controller (rev 06)
    00:1c.0 SD Host controller: Intel Corporation Celeron/Pentium Silver Processor SDA Standard Compliant SD Host Controller (rev 06)
    00:1f.0 ISA bridge: Intel Corporation Celeron/Pentium Silver Processor PCI-default ISA-bridge (rev 06)
    00:1f.1 SMBus: Intel Corporation Celeron/Pentium Silver Processor Gaussian Mixture Model (rev 06)
    ```

-   `hp-x360-12b-ca0500na-n4000-octopus`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      CPU [empty]
    /0/1                   memory         4GiB System memory
    /0/1/0                 memory         1GiB LPDDR4 Synchronous 2400 MHz (0.4 ns)
    /0/1/1                 memory         1GiB LPDDR4 Synchronous 2400 MHz (0.4 ns)
    /0/1/2                 memory         1GiB LPDDR4 Synchronous 2400 MHz (0.4 ns)
    /0/1/3                 memory         1GiB LPDDR4 Synchronous 2400 MHz (0.4 ns)
    /0/2                   processor      Intel(R) Celeron(R) N4000 CPU @ 1.10GHz
    /0/100                 bridge         Gemini Lake Host Bridge
    /0/100/2               display        GeminiLake [UHD Graphics 600]
    /0/100/c               network        AC 1550i Wireless
    /0/100/1f              bridge         Celeron/Pentium Silver Processor PCI-defau
    /1             eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Gemini Lake Host Bridge (rev 03)
    00:00.1 Signal processing controller: Intel Corporation Celeron/Pentium Silver Processor Dynamic Platform and Thermal Framework Processor Participant (rev 03)
    00:02.0 VGA compatible controller: Intel Corporation GeminiLake [UHD Graphics 600] (rev 03)
    00:0c.0 Network controller: Intel Corporation AC 1550i Wireless (rev 03)
    00:0e.0 Multimedia audio controller: Intel Corporation Celeron/Pentium Silver Processor High Definition Audio (rev 03)
    00:15.0 USB controller: Intel Corporation Celeron/Pentium Silver Processor USB 3.0 xHCI Controller (rev 03)
    00:17.0 Signal processing controller: Intel Corporation Device 31b4 (rev 03)
    00:17.1 Signal processing controller: Intel Corporation Device 31b6 (rev 03)
    00:17.2 Signal processing controller: Intel Corporation Device 31b8 (rev 03)
    00:17.3 Signal processing controller: Intel Corporation Device 31ba (rev 03)
    00:18.0 Signal processing controller: Intel Corporation Celeron/Pentium Silver Processor Serial IO UART Host Controller (rev 03)
    00:18.2 Signal processing controller: Intel Corporation Celeron/Pentium Silver Processor Serial IO UART Host Controller (rev 03)
    00:19.0 Signal processing controller: Intel Corporation Celeron/Pentium Silver Processor Serial IO SPI Host Controller (rev 03)
    00:19.2 Signal processing controller: Intel Corporation Celeron/Pentium Silver Processor Serial IO SPI Host Controller (rev 03)
    00:1c.0 SD Host controller: Intel Corporation Celeron/Pentium Silver Processor SDA Standard Compliant SD Host Controller (rev 03)
    00:1f.0 ISA bridge: Intel Corporation Celeron/Pentium Silver Processor PCI-default ISA-bridge (rev 03)
    00:1f.1 SMBus: Intel Corporation Celeron/Pentium Silver Processor Gaussian Mixture Model (rev 03)
    ```
