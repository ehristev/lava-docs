---
title: Strongbad Chromebooks
---

`strongbad` is a board name for ARM-64 Chromebooks using an Qualcomm Snapdragon 7c. It's an evolution of `trogdor`.

The Collabora LAVA lab contains the following `strongbad` devices:

-   [HP Fortis 11 G9 Q Chromebook](https://www.hp.com/us-en/shop/pdp/hp-fortis-11-inch-g9-q-chromebook) (codename `kingoftown`)
    -   Label: `TLA, NOTEBOOK, CHROMEBOOK, KINGOFTOWN, EVT, SKU2, UK, QUALCOMM QSIP-7180, 8GB RAM, 64GB, 11.6IN, HD, WIFI, BT, LTE`
    -   See [sc7180-trogdor-kingoftown](https://lava.collabora.dev/scheduler/device_type/sc7180-trogdor-kingoftown) in LAVA
    -   CPU: Qualcomm Kryo 468
    -   GPU: Qualcomm Adreno 618 GPU
    -   SoC: Qualcomm Snapdragon 7c
    -   Arch: Aarch64

Full SoC specs: <https://www.qualcomm.com/products/application/mobile-computing/snapdragon-8-series-mobile-compute-platforms/snapdragon-7c-compute-platform>

Mainline Linux kernel support was added in release v6.0-rc1: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/arch/arm64/boot/dts/qcom/sc7180-trogdor-kingoftown-r1.dts?h=v6.0-rc1>

### Debugging interfaces

`strongbad` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

In the HP Fortis 11 G9 Q Chromebook, the debug port is the USB-C port on the left side.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through either the Techrise USB-Eth adapter (R8152-based) or the Asix AX88772B AX88772 USB-Eth adapter.

#### Known issues

The same issues observed on `trogdor` are manifesting on `strongbad`:

-   Serial is very racy, often causing interference between kernel messages and LAVA signals (even when redirected to `/dev/kmsg`). The root cause of this issue is not yet known.
-   The driver for the onboard USB controller, when compiled as a module, is loaded too late in the boot process, preventing the IP from being assigned in time for mounting the NFS.

See also [Common issues](../common_issues.md).

### Example kernel command line arguments

An example of kernel command line arguments to boot a FIT image:

    console=ttyMSM0,115200n8 root=/dev/ram0 ip=dhcp tftpserverip=<server_ip>

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `kingoftown` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/trogdor-kingoftown.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.
