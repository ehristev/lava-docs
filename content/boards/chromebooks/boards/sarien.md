---
title: Sarien Chromebooks
---

`sarien` is a board name for x86_64-based Chromebooks. Some examples are:

-   [Dell Latitude 5400 Chromebook Enterprise](https://www.dell.com/en-us/work/shop/dell-laptops-and-notebooks/dell-latitude-5400-chromebook-enterprise/spd/latitude-14-5400-chrome-laptop/xctolc540014us1)
-   [Dell Latitude 5300 2-in-1 Chromebook Enterprise](https://www.dell.com/en-us/work/shop/cty/pdp/spd/latitude-13-5300-2-in-1-chrome-laptop)

The specs for these chromebooks vary in terms of format, display, connectivity
and devices, but they are based on Intel Whiskey Lake 64 bit CPUs
such as the Celeron 4305U.

The Collabora LAVA lab contains the following `sarien` devices:

-   [Dell Latitude 5400 Chromebook Enterprise](https://www.dell.com/en-us/work/shop/dell-laptops-and-notebooks/dell-latitude-5400-chromebook-enterprise/spd/latitude-14-5400-chrome-laptop/xctolc540014us1) (codename `sarien`)
    -   See [dell-latitude-5400-4305U-sarien](https://lava.collabora.dev/scheduler/device_type/dell-latitude-5400-4305U-sarien)
    -   CPU: Intel Celeron CPU 4305U @ 2.20GHz
    -   GPU: Intel UHD Graphics 610
    -   Aarch: x86_64
-   [Dell Latitude 5400 Chromebook Enterprise](https://www.dell.com/en-us/work/shop/dell-laptops-and-notebooks/dell-latitude-5400-chromebook-enterprise/spd/latitude-14-5400-chrome-laptop/xctolc540014us1) (codename `sarien`)
    -   See [dell-latitude-5400-8665U-sarien](https://lava.collabora.dev/scheduler/device_type/dell-latitude-5400-8665U-sarien) (Celeron 8665U CPU) in LAVA
    -   CPU: Intel Core i7-8665U CPU @ 1.90GHz
    -   GPU: Intel Corporation WhiskeyLake-U GT2 [UHD Graphics 620]
    -   Aarch: x86_64

### Debugging interfaces

The Dell Latitude 5400 Chromebook Enterprise has been flashed and tested
with a [Servo Micro](../../01-debugging_interfaces) interface and a
RTL8152-based USB-Ethernet adapter, as well as with a combination of
Servo Micro and Servo v4 as documented in [this
page](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_micro.md). Note
that `sarien` Chromebooks don't support [CCD](../../02-ccd).

#### Network connectivity

An external USB-Ethernet dongle based on the Realtek RTL8152 can be used
to provide Ethernet connectivity in Depthcharge. Alternatively, A Servo v4 can be
used together with the Servo Micro to provide an Ethernet interface (see
the [official
docs](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_micro.md)).

Note that to be able to use the RTL8152 USB-Eth adapter, the kernel must
be configured with `CONFIG_USB_RTL8152=y`.

#### Known issues

After issuing a `cold_reset:on` command with `dut-control`, this device
can't be turned on again with `cold_reset:off`. The recommended way to
turn it off and on is with the `power_state:off` and `power_state:on`
commands.

Flashing with the firmware-tools requires some modifications before flashing will work. The servoflash.py file needs changing so that the flashing voltage is 3.3 volts. This can be achieved by changing line 47 from:

    def _spi2_vref(self, status, voltage="pp1800"):

to

    def _spi2_vref(self, status, voltage="pp3300"):

See [Common issues](../common_issues.md) as well.

### Example kernel command line arguments

    earlyprintk=uart8250,mmio32,0xde000000,115200n8 console_msg_format=syslog console=ttyS0,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `sarien` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/sarien.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `dell-latitude-5400-4305U-sarien`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      CPU [empty]
    /0/4/6                 memory         32KiB L1 cache
    /0/4/7                 memory         256KiB L2 cache
    /0/4/8                 memory         2MiB L3 cache
    /0/5                   memory         32KiB L1 cache
    /0/1                   memory         4GiB System memory
    /0/1/0                 memory         4GiB DDR4 Synchronous 2133 MHz (0.5 ns)
    /0/2                   processor      Intel(R) Celeron(R) CPU 4305U @ 2.20GHz
    /0/100                 bridge         Coffee Lake Host Bridge/DRAM Registers
    /0/100/2               display        Intel Corporation
    /0/100/14.2            memory         RAM memory
    /0/100/14.3            network        Cannon Point-LP CNVi [Wireless-AC]
    /0/100/1c              bridge         Cannon Point PCI Express Root Port #8
    /0/100/1d              bridge         Cannon Point-LP PCI Express Root Port #13
    /0/100/1f              bridge         Cannon Point-LP LPC Controller
    /0/100/1f.6    eth0    network        Ethernet Connection (6) I219-V
    /1             eth1    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Coffee Lake Host Bridge/DRAM Registers (rev 0c)
    00:02.0 VGA compatible controller: Intel Corporation Device 3ea1 (rev 02)
    00:04.0 Signal processing controller: Intel Corporation Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor Thermal Subsystem (rev 0c)
    00:08.0 System peripheral: Intel Corporation Xeon E3-1200 v5/v6 / E3-1500 v5 / 6th/7th/8th Gen Core Processor Gaussian Mixture Model
    00:12.0 Signal processing controller: Intel Corporation Cannon Point-LP Thermal Controller (rev 30)
    00:14.0 USB controller: Intel Corporation Cannon Point-LP USB 3.1 xHCI Controller (rev 30)
    00:14.2 RAM memory: Intel Corporation Cannon Point-LP Shared SRAM (rev 30)
    00:14.3 Network controller: Intel Corporation Cannon Point-LP CNVi [Wireless-AC] (rev 30)
    00:15.0 Serial bus controller [0c80]: Intel Corporation Cannon Point-LP Serial IO I2C Controller #0 (rev 30)
    00:15.1 Serial bus controller [0c80]: Intel Corporation Cannon Point-LP Serial IO I2C Controller #1 (rev 30)
    00:16.0 Communication controller: Intel Corporation Cannon Point-LP MEI Controller #1 (rev 30)
    00:17.0 SATA controller: Intel Corporation Cannon Point-LP SATA Controller [AHCI Mode] (rev 30)
    00:19.0 Serial bus controller [0c80]: Intel Corporation Cannon Point-LP Serial IO I2C Host Controller (rev 30)
    00:19.2 Communication controller: Intel Corporation Device 9dc7 (rev 30)
    00:1c.0 PCI bridge: Intel Corporation Cannon Point PCI Express Root Port #8 (rev f0)
    00:1d.0 PCI bridge: Intel Corporation Cannon Point-LP PCI Express Root Port #13 (rev f0)
    00:1f.0 ISA bridge: Intel Corporation Cannon Point-LP LPC Controller (rev 30)
    00:1f.3 Audio device: Intel Corporation Cannon Point-LP High Definition Audio Controller (rev 30)
    00:1f.4 SMBus: Intel Corporation Cannon Point-LP SMBus Controller (rev 30)
    00:1f.5 Serial bus controller [0c80]: Intel Corporation Cannon Point-LP SPI Controller (rev 30)
    00:1f.6 Ethernet controller: Intel Corporation Ethernet Connection (6) I219-V (rev 30)
    01:00.0 Unassigned class [ff00]: Realtek Semiconductor Co., Ltd. RTS525A PCI Express Card Reader (rev 01)
    02:00.0 Non-Volatile memory controller: Solid State Storage Technology Corporation Device 9100 (rev 03)
    ```

-   `dell-latitude-5400-8665U-sarien`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      CPU [empty]
    /0/4/6                 memory         32KiB L1 cache
    /0/4/7                 memory         256KiB L2 cache
    /0/4/8                 memory         8MiB L3 cache
    /0/5                   memory         32KiB L1 cache
    /0/1                   memory         8GiB System memory
    /0/1/0                 memory         8GiB DDR4 Synchronous 2400 MHz (0.4 ns)
    /0/2                   processor      Intel(R) Core(TM) i7-8665U CPU @ 1.90GHz
    /0/100                 bridge         Coffee Lake HOST and DRAM Controller
    /0/100/2               display        WhiskeyLake-U GT2 [UHD Graphics 620]
    /0/100/14.2            memory         RAM memory
    /0/100/14.3            network        Cannon Point-LP CNVi [Wireless-AC]
    /0/100/1c              bridge         Cannon Point PCI Express Root Port #8
    /0/100/1d              bridge         Cannon Point-LP PCI Express Root Port #13
    /0/100/1f              bridge         Cannon Point-LP LPC Controller
    /0/100/1f.6    eth0    network        Ethernet Connection (6) I219-V
    /1             eth1    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Coffee Lake HOST and DRAM Controller (rev 0c)
    00:02.0 VGA compatible controller: Intel Corporation WhiskeyLake-U GT2 [UHD Graphics 620] (rev 02)
    00:04.0 Signal processing controller: Intel Corporation Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor Thermal Subsystem (rev 0c)
    00:08.0 System peripheral: Intel Corporation Xeon E3-1200 v5/v6 / E3-1500 v5 / 6th/7th/8th Gen Core Processor Gaussian Mixture Model
    00:12.0 Signal processing controller: Intel Corporation Cannon Point-LP Thermal Controller (rev 30)
    00:14.0 USB controller: Intel Corporation Cannon Point-LP USB 3.1 xHCI Controller (rev 30)
    00:14.2 RAM memory: Intel Corporation Cannon Point-LP Shared SRAM (rev 30)
    00:14.3 Network controller: Intel Corporation Cannon Point-LP CNVi [Wireless-AC] (rev 30)
    00:15.0 Serial bus controller [0c80]: Intel Corporation Cannon Point-LP Serial IO I2C Controller #0 (rev 30)
    00:15.1 Serial bus controller [0c80]: Intel Corporation Cannon Point-LP Serial IO I2C Controller #1 (rev 30)
    00:16.0 Communication controller: Intel Corporation Cannon Point-LP MEI Controller #1 (rev 30)
    00:17.0 SATA controller: Intel Corporation Cannon Point-LP SATA Controller [AHCI Mode] (rev 30)
    00:19.0 Serial bus controller [0c80]: Intel Corporation Cannon Point-LP Serial IO I2C Host Controller (rev 30)
    00:19.2 Communication controller: Intel Corporation Device 9dc7 (rev 30)
    00:1c.0 PCI bridge: Intel Corporation Cannon Point PCI Express Root Port #8 (rev f0)
    00:1d.0 PCI bridge: Intel Corporation Cannon Point-LP PCI Express Root Port #13 (rev f0)
    00:1f.0 ISA bridge: Intel Corporation Cannon Point-LP LPC Controller (rev 30)
    00:1f.3 Audio device: Intel Corporation Cannon Point-LP High Definition Audio Controller (rev 30)
    00:1f.4 SMBus: Intel Corporation Cannon Point-LP SMBus Controller (rev 30)
    00:1f.5 Serial bus controller [0c80]: Intel Corporation Cannon Point-LP SPI Controller (rev 30)
    00:1f.6 Ethernet controller: Intel Corporation Ethernet Connection (6) I219-V (rev 30)
    01:00.0 Unassigned class [ff00]: Realtek Semiconductor Co., Ltd. RTS525A PCI Express Card Reader (rev 01)
    02:00.0 SD Host controller: O2 Micro, Inc. Device 8620 (rev 01)
    ```
